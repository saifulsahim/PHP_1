<?php

// Total 8 data types

//      4 data types are scalar types

//      boolean


        $myTestVar = true;
        var_dump($myTestVar);

        echo "<br>";

//      integer


        $myTestVar = 64;
        var_dump($myTestVar);
        echo "<br>";

//   Single quoted  String


        $myTestVar = 'Hello Single quoted String';
        var_dump($myTestVar);

        echo "<br>";

//   Double quoted  String


        $myTestVar = "Hello Single quoted String";
        var_dump($myTestVar);

        echo "<br>";

// 2 compound data types
// Associative Array

        $person = array(
                "Abul"=>array("fullName"=>"Abul hossain","Age"=>45,"height"=>5.7),
                "Moyna"=>array("fullName"=>"Moyna Begum","Age"=>55,"height"=>4.7),
                "Jorina"=>array("fullName"=>"jorina begum","Age"=>25,"height"=>4.5),
                "kamal"=>array("fullName"=>"kamal hossain","Age"=>35,"height"=>5.9)
        );

     foreach($person as $nick=>$personInfo){

           echo"$nick:<br>";

             foreach($personInfo as $key=>$value{

                     echo " $key:".$value. "<br>";

             }
             echo "<br>";
     }


